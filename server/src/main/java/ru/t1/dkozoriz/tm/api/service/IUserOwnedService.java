package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.List;

public interface IUserOwnedService<T extends UserOwnedModel> extends IAbstractService<T> {

    void clear(@Nullable String userId) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<T> findAll(@Nullable String userId) throws Exception;

    @Nullable
    T findById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    T findByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    T removeById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    T removeByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @Nullable
    T remove(@Nullable String userId, @Nullable T model) throws Exception;
}