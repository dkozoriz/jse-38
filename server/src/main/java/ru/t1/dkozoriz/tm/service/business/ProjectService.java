package ru.t1.dkozoriz.tm.service.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.business.IBusinessRepository;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.business.IProjectService;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.repository.business.ProjectRepository;

import java.sql.Connection;

public final class ProjectService extends BusinessService<Project, IProjectRepository> implements IProjectService {

    private final static String NAME = "Project";

    @NotNull
    protected String getName() {
        return NAME;
    }

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = getRepository(connection);
        try {
            repository.add(userId, project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }


}