package ru.t1.dkozoriz.tm.repository.business;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.util.DBConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends BusinessRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return DBConstants.TABLE_PROJECT;
    }

    @NotNull
    @Override
    public Project fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DBConstants.COLUMN_ID));
        project.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        project.setName(row.getString(DBConstants.COLUMN_NAME));
        project.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        project.setStatus(Status.valueOf(row.getString(DBConstants.COLUMN_STATUS)));
        project.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_NAME, DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_STATUS,
                DBConstants.COLUMN_CREATED
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getUserId());
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getStatus().toString());
            statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
            statement.executeUpdate();
        }
        return project;
    }

    @Override
    @NotNull
    public Project add(@NotNull String userId, @NotNull Project project) throws Exception {
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public void update(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_STATUS, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getUserId());
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getStatus().toString());
            statement.setString(5, project.getId());
            statement.executeUpdate();
        }
    }

}