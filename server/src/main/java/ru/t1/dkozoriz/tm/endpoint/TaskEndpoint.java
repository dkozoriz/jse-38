package ru.t1.dkozoriz.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.dto.request.task.*;
import ru.t1.dkozoriz.tm.dto.response.task.*;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.model.business.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;


@WebService(endpointInterface = "ru.t1.dkozoriz.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowListTaskResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowListRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Task> taskList =
                getServiceLocator().getTaskService().findAll(userId, sort);
        return new ShowListTaskResponse(taskList);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public BindTaskToProjectResponse taskBindToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task =
                getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new BindTaskToProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UnbindTaskToProjectResponse taskUnbindToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindToProjectRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Task task =
                getServiceLocator().getProjectTaskService().unbindTaskToProject(userId, projectId, taskId);
        return new UnbindTaskToProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowAllTasksByProjectIdResponse taskShowAllByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowAllByProjectIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<Task> taskList = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
        return new ShowAllTasksByProjectIdResponse(taskList);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ChangeTaskStatusByIdResponse taskChangeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusById(userId, taskId, status);
        return new ChangeTaskStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ChangeTaskStatusByIndexResponse taskChangeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusByIndex(userId, taskIndex, status);
        return new ChangeTaskStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public CompleteTaskByIdResponse taskCompleteById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.COMPLETED);
        return new CompleteTaskByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public CompleteTaskByIndexResponse taskCompleteByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusByIndex(userId, taskIndex, Status.COMPLETED);
        return new CompleteTaskByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public StartTaskByIdResponse taskStartById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusById(userId, taskId, Status.IN_PROGRESS);
        return new StartTaskByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public StartTaskByIndexResponse taskStartByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Task task =
                getServiceLocator().getTaskService().changeStatusByIndex(userId, taskIndex, Status.IN_PROGRESS);
        return new StartTaskByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public CreateTaskResponse taskCreate(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task =
                getServiceLocator().getTaskService().create(userId, name, description);
        return new CreateTaskResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ListClearTaskResponse taskListClear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListClearRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        getServiceLocator().getTaskService().clear(userId);
        return new ListClearTaskResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public RemoveTaskByIdResponse taskRemoveById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getId();
        getServiceLocator().getTaskService().removeById(userId, taskId);
        return new RemoveTaskByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public RemoveTaskByIndexResponse taskRemoveByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        getServiceLocator().getTaskService().removeByIndex(userId, taskIndex);
        return new RemoveTaskByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowTaskByIdResponse taskShowById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Task task =
                getServiceLocator().getTaskService().findById(userId, taskId);
        return new ShowTaskByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowTaskByIndexResponse taskShowByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Task task =
                getServiceLocator().getTaskService().findByIndex(userId, taskIndex);
        return new ShowTaskByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UpdateTaskByIdResponse taskUpdateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task =
                getServiceLocator().getTaskService().updateById(userId, taskId, name, description);
        return new UpdateTaskByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UpdateTaskByIndexResponse taskUpdateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task =
                getServiceLocator().getTaskService().updateByIndex(userId, taskIndex, name, description);
        return new UpdateTaskByIndexResponse(task);
    }

}