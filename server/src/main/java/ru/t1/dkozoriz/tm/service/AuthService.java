package ru.t1.dkozoriz.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.service.*;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.exception.system.AccessDeniedException;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginErrorException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.util.CryptUtil;
import ru.t1.dkozoriz.tm.util.HashUtil;

import java.util.Date;


public final class AuthService implements IAuthService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public AuthService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        return serviceLocator.getUserService().create(login, password, email);
    }

    public String login(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new LoginErrorException();
        final boolean locked = user.getLocked();
        if (locked) throw new LoginErrorException();
        @Nullable final String hash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (hash == null) throw new LoginErrorException();
        if (!hash.equals(user.getPasswordHash())) throw new LoginErrorException();
        return getToken(user);
    }

    @Override
    @NotNull
    @SneakyThrows
    public Session validateToken(@Nullable String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = serviceLocator.getPropertyService().getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = serviceLocator.getPropertyService().getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (serviceLocator.getSessionService().findById(session.getId()) == null) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void invalidate(@Nullable Session session) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionService().remove(session);
    }

    @NotNull
    private String getToken(@NotNull final User user) throws Exception {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = serviceLocator.getPropertyService().getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @Nullable
    private Session createSession(@NotNull final User user) throws Exception {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return serviceLocator.getSessionService().add(session);
    }

}