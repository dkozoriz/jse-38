package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.api.service.IAbstractService;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class AbstractService<T extends AbstractModel, R extends IAbstractRepository<T>> implements IAbstractService<T> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected abstract IAbstractRepository<T> getRepository(@NotNull final Connection connection);

    @NotNull
    protected abstract String getName();

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @Nullable
    @Override
    public T add(@Nullable final T model) throws Exception {
        if (model == null) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final IAbstractRepository<T> repository = getRepository(connection);
        try {
            repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    public void update(@NotNull final T model) throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IAbstractRepository<T> repository = getRepository(connection);
        try {
            repository.update(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    public void clear() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IAbstractRepository<T> repository = getRepository(connection);
        try {
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    public List<T> findAll() throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<T> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @Nullable
    public void remove(@Nullable final T model) throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IAbstractRepository<T> repository = getRepository(connection);
        try {
            repository.remove(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final T model = findById(id);
        remove(model);
    }

    @Nullable
    public T findById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<T> repository = getRepository(connection);
            return repository.findById(id);
        }
    }

    public int getSize() throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IAbstractRepository<T> repository = getRepository(connection);
            return repository.getSize();
        }
    }

    @NotNull
    @Override
    public Collection<T> add(@Nullable final Collection<T> models) throws Exception {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final IAbstractRepository<T> repository = getRepository(connection);
        try {
            repository.add(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    public Collection<T> set(@Nullable final Collection<T> models) throws Exception {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final IAbstractRepository<T> repository = getRepository(connection);
        try {
            repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

}