package ru.t1.dkozoriz.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.endpoint.IUserEndpoint;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.dto.request.user.*;
import ru.t1.dkozoriz.tm.dto.response.user.*;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.dkozoriz.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public LockUserResponse userLock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getServiceLocator().getUserService().lockUserByLogin(login);
        return new LockUserResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UnlockUserResponse userUnlock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getServiceLocator().getUserService().unLockUserByLogin(login);
        return new UnlockUserResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public FindUserByLoginResponse userFindByLogin(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserFindByLoginRequest request
    ) {
        checkPermission(request);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getServiceLocator().getUserService().findByLogin(login);
        return new FindUserByLoginResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public RemoveUserResponse userRemove(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getServiceLocator().getUserService().removeByLogin(login);
        return new RemoveUserResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ChangePasswordUserResponse userChangePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final User user = getServiceLocator().getUserService().setPassword(userId, password);
        return new ChangePasswordUserResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UpdateProfileUserResponse userUpdateProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final User user = getServiceLocator().getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UpdateProfileUserResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public RegistryUserResponse userRegistry(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String email = request.getEmail();
        @Nullable final String password = request.getPassword();
        @Nullable final User user = getServiceLocator().getUserService().create(login, password, email);
        return new RegistryUserResponse(user);
    }

}