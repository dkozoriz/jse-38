package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface IAbstractService<T> {

    void update(@NotNull T model) throws Exception;

    @Nullable
    T add(@Nullable T model) throws Exception;

    @NotNull
    T findById(@Nullable String id) throws Exception;

    @Nullable
    void removeById(@Nullable String id) throws Exception;

    void clear() throws Exception;

    int getSize() throws Exception;

    @NotNull
    List<T> findAll() throws Exception;

    @Nullable
    void remove(@Nullable T model) throws Exception;

    @NotNull
    Collection<T> add(@NotNull Collection<T> models) throws Exception;

    @NotNull
    Collection<T> set(@NotNull Collection<T> models) throws Exception;

}