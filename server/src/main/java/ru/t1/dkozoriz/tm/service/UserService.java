package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.IUserService;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.EmailEmptyException;
import ru.t1.dkozoriz.tm.exception.field.EmailIsExistException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.RoleEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginIsExistException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.repository.UserRepository;
import ru.t1.dkozoriz.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final static String NAME = "User";

    @NotNull
    private final IServiceLocator serviceLocator;

    public UserService(@NotNull IConnectionService connectionService, @NotNull IServiceLocator serviceLocator) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    protected IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(serviceLocator.getPropertyService(), password));
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new EmailIsExistException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(serviceLocator.getPropertyService(), password));
        user.setEmail(email);
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(serviceLocator.getPropertyService(), password));
        user.setRole(role);
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }


    @Nullable
    public User findByLogin(@Nullable final String login) throws Exception{
        if (login == null || login.isEmpty()) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByLogin(login);
        }
    }

    @Nullable
    public User findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByEmail(email);
        }
    }

    public void remove(@Nullable final User user) throws Exception {
        if (user == null) throw new EntityException(getName());
        @NotNull final String userId = user.getId();
        super.remove(user);
        serviceLocator.getProjectService().clear(userId);
        serviceLocator.getTaskService().clear(userId);
        serviceLocator.getSessionService().clear(userId);
    }

    public void removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        remove(user);
    }

    public void removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new EntityException(getName());
        remove(user);
    }

    @NotNull
    public User setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findById(id.trim());
        if (user == null) throw new EntityException(getName());
        user.setPasswordHash(HashUtil.salt(serviceLocator.getPropertyService(), password));
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    public User updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName
    ) throws Exception {
        @Nullable final User user = findById(id);
        if (user == null) throw new EntityException(getName());
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    public Boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        return repository.isLoginExist(login);
    }

    @NotNull
    public Boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        return repository.isEmailExist(email);
    }

    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        user.setLocked(true);
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        repository.update(user);
    }

    public void unLockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        user.setLocked(false);
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        repository.update(user);
    }

}