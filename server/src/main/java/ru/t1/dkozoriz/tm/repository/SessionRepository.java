package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.ISessionRepository;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.util.DBConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends UserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return DBConstants.TABLE_SESSION;
    }

    @Override
    @NotNull
    public Session fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(row.getString(DBConstants.COLUMN_ID));
        session.setDate(row.getTimestamp(DBConstants.COLUMN_CREATED));
        session.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        if (row.getString(DBConstants.COLUMN_ROLE) != null)
            session.setRole(Role.valueOf(row.getString(DBConstants.COLUMN_ROLE)));
        else session.setRole(null);
        return session;
    }

    @Override
    @NotNull
    public Session add(@NotNull Session model) throws Exception {
        @NotNull final String sql = String.format("INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_CREATED, DBConstants.COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getUserId());
            statement.setTimestamp(3, new Timestamp(model.getDate().getTime()));
            statement.setString(4, model.getRole().toString());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @NotNull
    public Session add(@NotNull String userId, @NotNull Session session) throws Exception {
        session.setUserId(userId);
        return add(session);
    }

    @Override
    public void update(@NotNull Session model) throws Exception {

    }

}