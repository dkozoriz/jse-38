package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;
import ru.t1.dkozoriz.tm.util.DBConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class UserOwnedRepository<T extends UserOwnedModel> extends AbstractRepository<T>
        implements IUserOwnedRepository<T> {

    public UserOwnedRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public abstract T add(@NotNull final String userId, @NotNull final T model) throws Exception;

    public void clear(@Nullable final String userId) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), DBConstants.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    public boolean existById(@Nullable final String userId, @Nullable final String id) throws Exception {
        return findById(userId, id) != null;
    }

    @NotNull
    public List<T> findAll(@Nullable String userId) throws Exception {
        @NotNull final List<T> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s  WHERE %s = ?", getTableName(), DBConstants.COLUMN_USER_ID);
        try (@NotNull PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Nullable
    public T findById(@Nullable final String userId, @Nullable final String id) throws Exception {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ? LIMIT 1", getTableName(), DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            else return fetch(resultSet);
        }
    }

    @Nullable
    public T findByIndex(@Nullable final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT ?", getTableName(), DBConstants.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) resultSet.next();
            return fetch(resultSet);
        }
    }

    public int getSize(@Nullable final String userId) throws Exception {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE %s = ?", getTableName(), DBConstants.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    public void remove(@Nullable final String userId, @Nullable final T model) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ? AND %s = ?",
                getTableName(), DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, model.getId());
            statement.executeUpdate();
        }
    }

}