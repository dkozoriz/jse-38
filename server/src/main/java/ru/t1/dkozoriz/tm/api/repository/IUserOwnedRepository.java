package ru.t1.dkozoriz.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<T extends UserOwnedModel> extends IAbstractRepository<T> {

    @NotNull
    T add(@NotNull String userId, @NotNull T model) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    boolean existById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<T> findAll(@Nullable String userId) throws Exception;

    @Nullable
    T findById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    T findByIndex(@Nullable String userId, @NotNull Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    void remove(@Nullable String userId, @Nullable T model) throws Exception;

}