package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.ISessionRepository;
import ru.t1.dkozoriz.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.ISessionService;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.repository.SessionRepository;

import java.sql.Connection;

public final class SessionService extends UserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    protected String getName() {
        return "Session";
    }

    @Override
    @NotNull
    protected IUserOwnedRepository<Session> getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}