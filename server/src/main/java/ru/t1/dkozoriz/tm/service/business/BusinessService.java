package ru.t1.dkozoriz.tm.service.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.api.repository.business.IBusinessRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IUserOwnedService;
import ru.t1.dkozoriz.tm.api.service.business.IBusinessService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.field.StatusEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;
import ru.t1.dkozoriz.tm.service.UserOwnedService;

import java.sql.Connection;
import java.util.List;

public abstract class BusinessService<T extends BusinessModel, R extends IBusinessRepository<T>>
        extends UserOwnedService<T, R> implements IBusinessService<T> {

    public BusinessService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IBusinessRepository<T> getRepository(@NotNull final Connection connection);

    @NotNull
    public T changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final T model = findById(userId, id);
        if (model == null) throw new EntityException(getName());
        model.setStatus(status);
        update(model);
        return model;
    }

    @NotNull
    public T changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final T model = findByIndex(userId, index);
        if (model == null) throw new EntityException(getName());
        model.setStatus(status);
        update(model);
        return model;
    }

    @Nullable
    public List<T> findAll(@Nullable final String userId, @Nullable final Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IBusinessRepository<T> repository = getRepository(connection);
            return repository.findAll(userId, sort.getComparator());
        }
    }

    @NotNull
    public T updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final T model = findById(userId, id);
        if (model == null) throw new EntityException(getName());
        model.setName(name);
        model.setDescription(description);
        update(model);
        return model;
    }

    @NotNull
    public T updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final T model = findByIndex(userId, index);
        if (model == null) throw new EntityException(getName());
        model.setName(name);
        model.setDescription(description);
        update(model);
        return model;
    }

}