package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.model.User;

public interface IUserService extends IAbstractService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws Exception;

    void remove(@Nullable User model) throws Exception;

    void removeByLogin(@Nullable String login) throws Exception;

    void removeByEmail(@Nullable String email) throws Exception;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws Exception;

    Boolean isLoginExist(@Nullable String login) throws Exception;

    Boolean isEmailExist(@Nullable String email) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void unLockUserByLogin(@Nullable String login) throws Exception;

}