package ru.t1.dkozoriz.tm.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final BasicDataSource dataSource;


    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.dataSource = dataSource();
    }

    public BasicDataSource dataSource() {
        final BasicDataSource result = new BasicDataSource();
        result.setUrl(propertyService.getDBUrl());
        result.setUsername(propertyService.getDBUsername());
        result.setPassword(propertyService.getDBPassword());
        result.setMinIdle(5);
        result.setMaxIdle(10);
        result.setAutoCommitOnReturn(false);
        result.setMaxOpenPreparedStatements(100);
        return result;
    }


    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

}