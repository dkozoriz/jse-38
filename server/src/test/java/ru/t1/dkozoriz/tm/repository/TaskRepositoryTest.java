package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.marker.UnitCategory;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.repository.business.TaskRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final List<Task> taskList = new ArrayList<>();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task " + i);
            task.setDescription("Description " + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId(PROJECT_ID_1);
            }
            else {
                task.setUserId(USER_ID_2);
                task.setProjectId(PROJECT_ID_2);
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAddTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task task = new Task();
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testAddTaskUserOwned() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task task = new Task();
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        Assert.assertNull(taskRepository.add(null, task));
    }

    @Test
    public void testAddTaskCollection() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull List<Task> tasks = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskRepository.add(tasks);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClearTask() {
        final int expectedNumberOfEntries = 0;
        taskRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClearUserOwnedTask() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userTaskList.size();
        taskRepository.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testGetSizeTask() {
        Assert.assertEquals(taskList.size(), taskRepository.getSize());
    }

    @Test
    public void testGetSizeUserOwnedTask() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(userTaskList.size(), taskRepository.getSize(USER_ID_1));
    }

    @Test
    public void testSetTask() {
        @NotNull List<Task> tasks = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        final int expectedNumberOfEntries = tasks.size();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.set(tasks).size());
    }

    @Test
    public void testFindAllTask() {
        Assert.assertEquals(taskList.size(), taskRepository.findAll().size());
    }

    @Test
    public void testFindAllUserOwnedTask() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userTaskList.size();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.findAll(USER_ID_1).size());
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.findAll(USER_ID_1).size());
        Assert.assertEquals(Collections.emptyList(), taskRepository.findAll(null));
    }

    @Test
    public void testFindTaskById() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.findById(task.getId()));
            Assert.assertEquals(task, taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testFindTaskByIndex() {
        for (int i = 0; i < taskRepository.getSize(); i++) {
            Assert.assertNotNull(taskRepository.findByIndex(i));
            Assert.assertEquals(taskList.get(i), taskRepository.findByIndex(i));
        }
    }

    @Test
    public void testFindUserOwnedTaskById() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : userTaskList) {
            Assert.assertNotNull(taskRepository.findById(USER_ID_1, task.getId()));
            Assert.assertEquals(task, taskRepository.findById(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testFindUserOwnedTaskByIndex() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < userTaskList.size(); i++) {
            Assert.assertNotNull(taskRepository.findByIndex(USER_ID_1, i));
            Assert.assertEquals(taskList.get(i), taskRepository.findByIndex(USER_ID_1, i));
        }
    }

    @Test
    public void testFindAllByProjectId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskRepository.findAllByProjectId(null, PROJECT_ID_1).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskRepository.findAllByProjectId("", PROJECT_ID_1).size());
        @NotNull final List<Task> projectTaskList = taskList
                .stream()
                .filter(p -> PROJECT_ID_1.equals(p.getProjectId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = projectTaskList.size();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.findAllByProjectId(USER_ID_1, PROJECT_ID_1).size());
    }

    @Test
    public void testExistUserOwnedTaskById() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : userTaskList) {
            Assert.assertTrue(taskRepository.existById(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testRemoveTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = taskList.get(0);
        taskRepository.remove(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task task2 = null;
        Assert.assertNull(taskRepository.remove(task2));
    }

    @Test
    public void testRemoveTaskById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = taskList.get(0);
        taskRepository.removeById(task.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveTaskByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        taskRepository.removeByIndex(index);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveUserOwnedTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = taskList.get(0);
        taskRepository.remove(USER_ID_1, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        Assert.assertNull(taskRepository.remove(null, taskList.get(1)));
        Assert.assertNull(taskRepository.remove("", taskList.get(2)));
    }

    @Test
    public void testRemoveUserOwnedTaskById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = taskList.get(0);
        taskRepository.removeById(USER_ID_1, task.getId());
        Assert.assertNull(taskRepository.removeById(null, task.getId()));
        Assert.assertNull(taskRepository.removeById("", task.getId()));
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveUserOwnedTaskByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        taskRepository.removeByIndex(USER_ID_1, index);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

}