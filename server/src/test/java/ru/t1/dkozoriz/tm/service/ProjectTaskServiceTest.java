package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.IProjectTaskService;
import ru.t1.dkozoriz.tm.api.service.business.IProjectService;
import ru.t1.dkozoriz.tm.api.service.business.ITaskService;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.TaskIdEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.marker.UnitCategory;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.repository.business.ProjectRepository;
import ru.t1.dkozoriz.tm.repository.business.TaskRepository;
import ru.t1.dkozoriz.tm.service.business.ProjectService;
import ru.t1.dkozoriz.tm.service.business.TaskService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final List<Task> taskList = new ArrayList<>();

    @NotNull
    private final List<Project> projectList = new ArrayList<>();


    @Before
    public void initTask() {
        @Nullable Project project = projectService.create(USER_ID_1, "project", "description");
        projectList.add(project);

        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task " + i);
            task.setDescription("Description " + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId(projectList.get(0).getId());
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull Task task1 = taskService.create(USER_ID_1, "task1", "description");
        @NotNull String projectId = projectList.get(0).getId();
        @NotNull Task task = taskList.get(0);
        @NotNull String taskId = task.getId();
        task1.setProjectId(projectId);
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
        Assert.assertEquals(task1.getProjectId(), task.getProjectId());
    }

    @Test
    public void testBindTaskToProjectNegative() {
        @NotNull String projectId = projectList.get(0).getId();
        @NotNull String taskId = taskList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService
                .bindTaskToProject(null, projectId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService
                .bindTaskToProject("", projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService
                .bindTaskToProject(USER_ID_1, null, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService
                .bindTaskToProject(USER_ID_1, "", taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService
                .bindTaskToProject(USER_ID_1, projectId, null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService
                .bindTaskToProject(USER_ID_1, projectId, ""));
        Assert.assertThrows(EntityException.class, () -> projectTaskService
                .bindTaskToProject(USER_ID_1, "not exist id", taskId));
        Assert.assertThrows(EntityException.class, () -> projectTaskService
                .bindTaskToProject(USER_ID_1, projectId, "not exist id"));
    }

    @Test
    public void testUnbindTaskToProject() {
        @NotNull String projectId = projectList.get(0).getId();
        @NotNull Task task = taskList.get(0);
        @NotNull String taskId = task.getId();
        task.setProjectId(projectId);
        projectTaskService.unbindTaskToProject(USER_ID_1, projectId, taskId);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void testUnbindTaskToProjectNegative() {
        @NotNull String projectId = projectList.get(0).getId();
        @NotNull String taskId = taskList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService
                .unbindTaskToProject(null, projectId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService
                .unbindTaskToProject("", projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService
                .unbindTaskToProject(USER_ID_1, null, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService
                .unbindTaskToProject(USER_ID_1, "", taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService
                .unbindTaskToProject(USER_ID_1, projectId, null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService
                .unbindTaskToProject(USER_ID_1, projectId, ""));
        Assert.assertThrows(EntityException.class, () -> projectTaskService
                .unbindTaskToProject(USER_ID_1, "not exist id", taskId));
        Assert.assertThrows(EntityException.class, () -> projectTaskService
                .unbindTaskToProject(USER_ID_1, projectId, "not exist id"));
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final List<Task> projectTaskList = taskList
                .stream()
                .filter(p -> projectList.get(0).getId().equals(p.getProjectId()))
                .collect(Collectors.toList());
        projectTaskService.removeProjectById(USER_ID_1, projectList.get(0).getId());
        Assert.assertEquals(0, projectRepository.getSize());
        Assert.assertEquals(projectTaskList.size(), taskRepository.getSize());
    }

    @Test
    public void testRemoveProjectByIdNegative() {
        @NotNull String projectId = projectList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService
                .removeProjectById(null, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService
                .removeProjectById("", projectId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService
                .removeProjectById(USER_ID_1, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService
                .removeProjectById(USER_ID_1, ""));
        Assert.assertThrows(EntityException.class, () -> projectTaskService
                .removeProjectById(USER_ID_1, "not exist id"));
    }

}