package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.marker.UnitCategory;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.repository.business.ProjectRepository;

import java.util.*;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final List<Project> projectList = new ArrayList<>();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAddProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project project = new Project();
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testAddProjectUserOwned() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project project = new Project();
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertNull(projectRepository.add(null, project));
    }

    @Test
    public void testAddProjectCollection() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull List<Project> projects = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectRepository.add(projects);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClearProject() {
        final int expectedNumberOfEntries = 0;
        projectRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClearUserOwnedProject() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userProjectList.size();
        projectRepository.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testGetSizeProject() {
        Assert.assertEquals(projectList.size(), projectRepository.getSize());

    }

    @Test
    public void testGetSizeUserOwnedProject() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(userProjectList.size(), projectRepository.getSize(USER_ID_1));
        Assert.assertEquals(0, projectRepository.getSize(null));
        Assert.assertEquals(0, projectRepository.getSize(""));
    }

    @Test
    public void testSetProject() {
        @NotNull List<Project> projects = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        final int expectedNumberOfEntries = projects.size();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.set(projects).size());
    }

    @Test
    public void testFindAllProject() {
        Assert.assertEquals(projectList.size(), projectRepository.findAll().size());
    }

    @Test
    public void testFindAllUserOwnedProject() {
        Assert.assertEquals(Collections.emptyList(), projectRepository.findAll(null));
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userProjectList.size();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindAllUserOwnedProjectSort() {
        Assert.assertNull(projectRepository.findAll(null, Sort.toSort("BY_NAME").getComparator()));
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userProjectList.size();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.findAll(USER_ID_1, Sort.toSort("BY_NAME").getComparator()).size());
    }

    @Test
    public void testFindProjectById() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.findById(project.getId()));
            Assert.assertEquals(project, projectRepository.findById(project.getId()));
        }
    }

    @Test
    public void testFindProjectByIndex() {
        for (int i = 0; i < projectRepository.getSize(); i++) {
            Assert.assertNotNull(projectRepository.findByIndex(i));
            Assert.assertEquals(projectList.get(i), projectRepository.findByIndex(i));
        }
    }

    @Test
    public void testFindUserOwnedProjectById() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : userProjectList) {
            Assert.assertNotNull(projectRepository.findById(USER_ID_1, project.getId()));
            Assert.assertEquals(project, projectRepository.findById(USER_ID_1, project.getId()));
        }
        Assert.assertNull(projectRepository.findById(null, projectList.get(0).getId()));
        Assert.assertNull(projectRepository.findById("", projectList.get(0).getId()));
        Assert.assertNull(projectRepository.findById(USER_ID_1, null));
        Assert.assertNull(projectRepository.findById(USER_ID_1, ""));
    }

    @Test
    public void testFindUserOwnedProjectByIndex() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (int i = 0; i < userProjectList.size(); i++) {
            Assert.assertNotNull(projectRepository.findByIndex(USER_ID_1, i));
            Assert.assertEquals(projectList.get(i), projectRepository.findByIndex(USER_ID_1, i));
        }
    }

    @Test
    public void testExistUserOwnedProjectById() {
        @NotNull final List<Project> userProjectList = projectList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : userProjectList) {
            Assert.assertTrue(projectRepository.existById(USER_ID_1, project.getId()));
        }
        Assert.assertFalse(projectRepository.existById(USER_ID_1, null));
    }

    @Test
    public void testRemoveProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = projectList.get(0);
        projectRepository.remove(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project project2 = null;
        Assert.assertNull(projectRepository.remove(project2));
    }

    @Test
    public void testRemoveProjectById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = projectList.get(0);
        projectRepository.removeById(project.getId());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveProjectByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        projectRepository.removeByIndex(index);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveUserOwnedProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = projectList.get(0);
        projectRepository.remove(USER_ID_1, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertNull(projectRepository.remove(null, projectList.get(1)));
        Assert.assertNull(projectRepository.remove("", projectList.get(2)));
    }

    @Test
    public void testRemoveUserOwnedProjectById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = projectList.get(0);
        projectRepository.removeById(USER_ID_1, project.getId());
        Assert.assertNull(projectRepository.removeById(null, project.getId()));
        Assert.assertNull(projectRepository.removeById("", project.getId()));
        Assert.assertNull(projectRepository.removeById(USER_ID_1, "not exist id"));
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveUserOwnedProjectByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        projectRepository.removeByIndex(USER_ID_1, index);
        Assert.assertNull(projectRepository.removeByIndex(null, index));
        Assert.assertNull(projectRepository.removeByIndex("", index));
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

}