package ru.t1.dkozoriz.tm.command.data.save;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataBase64SaveRequest;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataXmlSaveJaxBRequest;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    public DataXmlSaveJaxBCommand() {
        super("data-save-xml-jaxb", "save data in xml file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        getEndpointLocator().getDomainEndpoint().saveXmlJaxB(new DataXmlSaveJaxBRequest(getToken()));

    }

}