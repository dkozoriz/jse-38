package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public AbstractUserCommand(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new EntityException("User");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    public String getArgument() {
        return null;
    }

}