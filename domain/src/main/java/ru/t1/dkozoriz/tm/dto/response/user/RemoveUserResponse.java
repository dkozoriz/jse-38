package ru.t1.dkozoriz.tm.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;
import ru.t1.dkozoriz.tm.model.User;

@Getter
@Setter
public class RemoveUserResponse extends AbstractResponse {

}